# Copyright 2021 Michele Vidotto <michele.vidotto@gmail.com

COMMA := ,

log:
	mkdir -p $@

# Other usefull params

# -r $(REFERENCE) \
# --references-list
# --conserved-genes-finding \
# --circos
# --blast-db

# calculate statistics on contigs for MegaHit
# scaffolds for metaspades
$(subst $(COMMA),-,$(LABELS))/report.html: log
	$(call load_env); \
	metaquast.py \
	--threads 32 \
	--min-contig $(MIN_CONTIG_LENGTH) \
	--max-ref-number 50 \
	--labels "$(LABELS)" \
	--k-mer-stats \
	--k-mer-size $(KMER_SIZE) \
	--pe1 $(PE_1) \
	--pe2 $(PE_2) \
	--single $(SE) \
	--plots-format png \
	--rna-finding \
	--output-dir $(dir $@) \
	$(CONTIGS) \
	2>&1 \
	| tee $</quast.$(subst $(COMMA),-,$(LABELS)).log


ALL += $(subst $(COMMA),-,$(LABELS))/report.html