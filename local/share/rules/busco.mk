# Copyright 2019 Michele Vidotto <michele.vidotto@gmail.com>

TEST_DATASET ?= 

tmp:
	mkdir -p $@

genome.fasta:
	cat <$(QUERY) >$@
	
proteins.fasta:
	zcat <$(QUERY_PROT) >$@

# check the configuration file for paths of all dependecy
# programs
run_$(SAMPLE).busco_genome: genome.fasta tmp
	!threads
	module purge; \
	module load tools/busco/3.02b; \
	run_BUSCO.py -i $< -c 8 -o $(subst run_,,$@) -m genome -l $(TEST_DATASET) -t $^2 --augustus_parameters --genemodel=complete



run_$(SAMPLE).busco_prot: proteins.fasta tmp
	module purge; \
	module load tools/busco/3.02b; \
	run_BUSCO.py -i $< -c 8 -o $@ -m prot -l $(TEST_DATASET) -t $^2

# generate plots using R script and ggplot2
# N.B.: must NOT be executed in screen mode
busco_plot: run_$(SAMPLE).busco_genome
	module purge; \
	module load tools/busco/3.02b; \
	mkdir -p $@; \
	cd $@; \
	cp ../run_$(SAMPLE).busco_genome/short_summary_$(SAMPLE).busco_genome.txt .; \
	generate_plot.py -wd $$PWD

	#cp ../run_$(SAMPLE).busco_prot/short_summary_$(SAMPLE).busco_prot.txt .; \


run_$(SAMPLE).busco_%.tar.pxz: 
	!threads
	module purge; \
	module load compression/pixz/1.0.2; \
	tar cvf - run_$(SAMPLE).busco_$* | pixz -p $$THREADNUM -9 >$@

.PHONY: test
	test


# ALL += run_$(SAMPLE).busco_genome \
	# run_$(SAMPLE).busco_prot \
	# busco_plot \
	# run_$(SAMPLE).busco_genome.tar.pxz \
	# run_$(SAMPLE).busco_prot.tar.pxz

ALL += run_$(SAMPLE).busco_genome.tar.pxz \
	run_$(SAMPLE).busco_prot.tar.pxz

INTERMEDIATE +=

CLEAN +=
