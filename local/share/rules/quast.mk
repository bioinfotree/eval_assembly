# Copyright 2019 Michele Vidotto <michele.vidotto@gmail.com

COMMA := ,

log:
	mkdir -p $@

# calculate statistics on contigs
$(subst $(COMMA),-,$(LABELS))/report.html: log
	!threads
	$(call load_env); \
	quast.py \
	--threads "$$THREADNUM" \
	--features gene:$(GENES) \
	-r $(REFERENCE) \
	--min-contig $(MIN_CONTIG_LENGTH) \
	--labels "$(LABELS)" \
	--conserved-genes-finding \
	--k-mer-stats \
	--k-mer-size $(KMER_SIZE) \
	--pe1 $(PE_1) \
	--pe2 $(PE_2) \
	--plots-format png \
	--rna-finding \
	--output-dir $(dir $@) \
	$(CONTIGS) \
	2>&1 \
	| tee $</quast.$(subst $(COMMA),-,$(LABELS)).log


ALL += $(subst $(COMMA),-,$(LABELS))/report.html


# calculate statistics on scaffolds
$(subst $(COMMA),-,$(LABELS))_scaffolds/report.html: log
	!threads
	$(call load_env); \
	quast.py \
	--threads "$$THREADNUM" \
	--features gene:$(GENES) \
	-r $(REFERENCE) \
	--min-contig $(MIN_CONTIG_LENGTH) \
	--labels "$(LABELS)" \
	--conserved-genes-finding \
	--k-mer-stats \
	--k-mer-size $(KMER_SIZE) \
	--pe1 $(PE_1) \
	--pe2 $(PE_2) \
	--plots-format png \
	--rna-finding \
	--output-dir $(dir $@) \
	$(SCAFFOLDS) \
	2>&1 \
	| tee $</quast.$(subst $(COMMA),-,$(LABELS)).log

	#export LD_LIBRARY_PATH=$(CONDA_ENV)/lib:$LD_LIBRARY_PATH; \

#ALL += $(subst $(COMMA),-,$(LABELS))_scaffolds/report.html