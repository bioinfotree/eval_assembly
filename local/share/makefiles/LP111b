# Copyright 2019 Michele Vidotto <michele.vidotto@gmail.com

# conda environment
CONDA_ENV := /mnt/vol2/conda/miniconda3

# load conda environment
define load_env
	module purge; \
	__conda_setup="$$(CONDA_REPORT_ERRORS=false '$(CONDA_ENV)/bin/conda' shell.bash hook 2> /dev/null)"; \
	if [ $$? -eq 0 ]; then \
		\eval "$$__conda_setup"; \
	else \
		if [ -f "$(CONDA_ENV)/etc/profile.d/conda.sh" ]; then \
			. "$(CONDA_ENV)/etc/profile.d/conda.sh"; \
			CONDA_CHANGEPS1=false conda activate base; \
		else \
			\export PATH="$(CONDA_ENV)/bin:$$PATH"; \
		fi; \
	fi; \
	conda activate "$$PRJ_ROOT/local/share/envs/eval_assembly"
endef

# reference
# genome
REFERENCE := /mnt/vol2/genomes/lpneumophila/assembly/reference/CP015949.1.fasta
# space separated list of contigs
CONTIGS :=  /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/allpaths_assembly/dataset/LP111b/phase_2/1bp.final.contigs.fasta.gz /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/allpaths_assembly/dataset/LP111b_100x/phase_2/1bp.final.contigs.fasta.gz /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/spades_assembly/dataset/LP111b/1bp.contigs.fasta.gz

# SCAFFOLDS := /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/allpaths_assembly/dataset/LP111b/phase_2/1bp.final.assembly.fasta.gz /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/spades_assembly/dataset/LST1/1bp.scaffolds.fasta.gz

# contigs of this min length
# are retained
MIN_CONTIG_LENGTH := 0
# comma separated labels of assemblies in
# same order as contig files
LABELS := allpaths,allpaths_100x,spades
# annotation file
# for the reference in
# GFF, BED, NCBI or TXT
GENES := /mnt/vol2/genomes/lpneumophila/annotation/genes/CP015949.1.gff3
# kmner used for
# kmerization
KMER_SIZE := 19
# paired end reads
# used for the assembly
# They will be aligned both to
# the reference and to the contigs
PE_1 := /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/illumina_preprocessing/dataset/LP/phase_2/ID1010_LP111b_S104_L001.cutadapt.erne_1.fastq.gz
PE_2 := /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/illumina_preprocessing/dataset/LP/phase_2/ID1010_LP111b_S104_L001.cutadapt.erne_2.fastq.gz