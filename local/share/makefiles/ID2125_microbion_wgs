# Copyright 2021 Michele Vidotto <michele.vidotto@gmail.com

# conda environment
CONDA_ENV := /igatech/rd/dev/miniconda3

# load conda environment
define load_env
	module purge; \
	__conda_setup="$$(CONDA_REPORT_ERRORS=false '$(CONDA_ENV)/bin/conda' shell.bash hook 2> /dev/null)"; \
	if [ $$? -eq 0 ]; then \
		\eval "$$__conda_setup"; \
	else \
		if [ -f "$(CONDA_ENV)/etc/profile.d/conda.sh" ]; then \
			. "$(CONDA_ENV)/etc/profile.d/conda.sh"; \
			CONDA_CHANGEPS1=false conda activate base; \
		else \
			\export PATH="$(CONDA_ENV)/bin:$$PATH"; \
		fi; \
	fi; \
	conda activate "$(CONDA_ENV)/envs/assembly_denovo"
endef

# reference
# genome
REFERENCE :=
# space separated list of contigs
CONTIGS := /igatech/rd/dev/packages/bioinfoconda/bioinfotree/prj/metaspades_assembly/dataset/ID2125_microbion_wgs/1-MB186/contigs.fasta \
           /igatech/rd/dev/packages/bioinfoconda/bioinfotree/prj/megahit_assembly/dataset/ID2125_microbion_wgs/1-MB186/out/final.contigs.fa

SCAFFOLDS :=

# contigs of this min length
# are retained
MIN_CONTIG_LENGTH := 0
# comma separated labels of assemblies in
# same order as contig files
LABELS := metaspades,megahit
# annotation file
# for the reference in
# GFF, BED, NCBI or TXT
GENES := 
# kmner used for
# kmerization
KMER_SIZE := 101
# paired end reads
# used for the assembly
# They will be aligned both to
# the reference and to the contigs
PE_1 := /igatech/workspace/pipeline_results/metagenomics-wgs-wts/2021/ID2125_Microbion_WGS/trim_align-erne_filter_bbduk_multiqc/trimmed/69822_ID2125_1-MB186_S1_L001_clean_1.fastq.gz
PE_2 := /igatech/workspace/pipeline_results/metagenomics-wgs-wts/2021/ID2125_Microbion_WGS/trim_align-erne_filter_bbduk_multiqc/trimmed/69822_ID2125_1-MB186_S1_L001_clean_2.fastq.gz