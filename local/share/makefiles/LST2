# Copyright 2019 Michele Vidotto <michele.vidotto@gmail.com

# conda environment
CONDA_ENV := /mnt/vol2/conda/miniconda3

# load conda environment
define load_env
	module purge; \
	__conda_setup="$$(CONDA_REPORT_ERRORS=false '$(CONDA_ENV)/bin/conda' shell.bash hook 2> /dev/null)"; \
	if [ $$? -eq 0 ]; then \
		\eval "$$__conda_setup"; \
	else \
		if [ -f "$(CONDA_ENV)/etc/profile.d/conda.sh" ]; then \
			. "$(CONDA_ENV)/etc/profile.d/conda.sh"; \
			CONDA_CHANGEPS1=false conda activate base; \
		else \
			\export PATH="$(CONDA_ENV)/bin:$$PATH"; \
		fi; \
	fi; \
	conda activate "$$PRJ_ROOT/local/share/envs/eval_assembly"
endef

# reference
# genome
REFERENCE := /mnt/vol2/genomes/lpneumophila/assembly/reference/CP015949.1.fasta
# space separated list of contigs
CONTIGS := /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/allpaths_assembly/dataset/LST2/phase_2/1bp.final.contigs.fasta.gz /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/spades_assembly/dataset/LST2/1bp.contigs.fasta.gz

# SCAFFOLDS := /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/allpaths_assembly/dataset/LST2/phase_2/1bp.final.assembly.fasta.gz /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/spades_assembly/dataset/LST2/1bp.scaffolds.fasta.gz

# contigs of this min length
# are retained
MIN_CONTIG_LENGTH := 0
# comma separated labels of assemblies in
# same order as contig files
LABELS := allpaths,spades
# annotation file
# for the reference in
# GFF, BED, NCBI or TXT
GENES := /mnt/vol2/genomes/lpneumophila/annotation/genes/CP015949.1.gff3
# kmner used for
# kmerization
KMER_SIZE := 19
# paired end reads
# used for the assembly
# They will be aligned both to
# the reference and to the contigs
PE_1 := /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/decontaminate/dataset/LST2/ID1010_LST2_S58_L001.1.mapped.fastq.gz
PE_2 := /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/lpneumophila/decontaminate/dataset/LST2/ID1010_LST2_S58_L001.2.mapped.fastq.gz